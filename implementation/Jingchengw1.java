//Random player first version
package implementation;

import java.io.PrintStream;
import java.util.ArrayList;

import aiproj.hexifence.Move;
import aiproj.hexifence.Piece;
import aiproj.hexifence.Player;

public class Jingchengw1 implements Player, Piece {

	private int dimension;
	private int colourNum;
	private char board[][];
	private int limit;
	private int boardSize;
	private int totalNumOfCell;
	private int numOfCellCaptured;

	public int init(int n, int p) {
		if ((n == 2 || n == 3) && (p == BLUE || p == RED)) {
			dimension = n;
			colourNum = p;
			limit = dimension * 2 - 1;
			boardSize = dimension * 4 - 1;
			totalNumOfCell = dimension * dimension * 2 - 1;
			numOfCellCaptured = 0;
			this.initBoard();
			return 0;
		} else {

			return -1;
		}
	}
	//player will make move when find available edge
	public Move makeMove() {
		Move m = new Move();
		m.P = this.colourNum;
		for (int i = 0; i < dimension * 4 - 1; i++) {
			for (int j = 0; j < dimension * 4 - 1; j++) {
				if (this.board[i][j] == '+') {
					this.board[i][j] = findColour(this.colourNum);
					m.Row = i;
					m.Col = j;
					if (this.canCapture(m)) {
						numOfCellCaptured++;
					}
					return m;
				}
			}
		}
		return null;

	}

	public int opponentMove(Move m) {
		int col = m.Col;
		int row = m.Row;
		if (board[row][col] == '-') {
			return -1;
		} else {
			board[row][col] = findColour(m.P);
			if (this.canCapture(m)) {
				return 1;
			}
			return 0;
		}

	}

	public int getWinner() {
		if (this.numOfCellCaptured >= ((this.totalNumOfCell + 1) / 2)) {

			return this.colourNum;
		} else {
			return 0;
		}
	}

	public void printBoard(PrintStream output) {
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				output.print(board[i][j] + "  ");
			}
			output.println();
		}
	}

	// below are the helping method

	private void initBoard() {

		this.board = new char[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				// center of a cell,invalid
				if ((i % 2 == 1) && (j % 2 == 1)) {
					board[i][j] = '-';
				}
				// larger than the limit,invalid
				else if (Math.abs(i - j) > limit) {
					board[i][j] = '-';
				} else {
					board[i][j] = '+';
				}
			}
		}

	}

	private boolean canCapture(Move m) {
		ArrayList<Move> centers = findCenters(m);
		for (Move tmp : centers) {
			if (this.cellCaptured(tmp)) {
				this.board[tmp.Row][tmp.Col] = Character.toLowerCase(findColour(m.P));
				return true;
			}

		}
		return false;

	}

	private ArrayList<Move> findCenters(Move m) {
		ArrayList<Move> centers = new ArrayList<Move>();

		for (int i = m.Row - 1; i < m.Row + 1; i++) {
			for (int j = m.Col - 1; j < m.Col + 1; j++) {
				if (i == m.Row && j == m.Col) {
					break;
				} else if (i < 0 || j < 0 || (i > boardSize - 1) || (j > boardSize - 1)) {
					break;
				}
				if (board[i][j] == '-' && (Math.abs(i - j) < limit)) {
					// center found
					Move tmp = new Move();
					tmp.Row = i;
					tmp.Col = j;
					centers.add(tmp);
				}
			}
		}
		return centers;
	}

	private boolean cellCaptured(Move m) {
		int aroundBoarding = 0;
		int row = m.Row;
		int col = m.Col;
		if (board[row - 1][col - 1] != '+') {
			aroundBoarding += 1;
		}
		if (board[row - 1][col] != '+') {
			aroundBoarding += 1;
		}
		if (board[row][col - 1] != '+') {
			aroundBoarding += 1;
		}
		if (board[row][col + 1] != '+') {
			aroundBoarding += 1;
		}
		if (board[row + 1][col] != '+') {
			aroundBoarding += 1;
		}
		if (board[row + 1][col + 1] != '+') {
			aroundBoarding += 1;
		}
		if (aroundBoarding == 6) {
			return true;
		} else {
			return false;
		}
	}

	private char findColour(int colourNum) {
		char colour;
		if(colourNum==BLUE)
		{
			colour='B';
		}
		else
		{
			colour='R';
		}
		return colour;

	}

}
